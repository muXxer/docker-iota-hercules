#!/bin/bash
# Written by muXxer

printf "\nStopping Hercules...\n"
docker-compose down
./download_latest_hercules_snapshot.sh
printf "Deleting old docker image...\n"
docker rmi -f $(docker images -qf "dangling=true") dockeriotahercules_hercules
printf "Deleting old Hercules database...\n"
sudo rm -Rf volumes/hercules/data
printf "Rebuilding Hercules image...\n"
docker-compose up -d