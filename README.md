# docker-iota-hercules

This repository contains the `docker-compose.yml` for CarrIOTA Hercules including Nelson.cli, Nelson.gui and Field.cli.<br>

## Table of contents
- [docker-iota-hercules](#docker-iota-hercules)
  * [Table of contents](#table-of-contents)
  * [1. WARNING](#1-warning)
  * [2. Install guide](#2-install-guide)
    + [2.1 Install Docker](#21-install-docker)
    + [2.2 Download this repository](#22-download-this-repository)
    + [2.3 Change the IOTA configuration files to fit your needs](#23-change-the-iota-configuration-files-to-fit-your-needs)
      - [2.3.1 Change the Nelson config.ini](#231-change-the-nelson-configini)
      - [2.3.2 Change the Field config.ini](#232-change-the-field-configini)
    + [2.4 Download the latest snapshot](#24-download-the-latest-snapshot)
    + [2.5 Open the following ports in your firewall for the IP address](#25-open-the-following-ports-in-your-firewall-for-the-ip-address)
  * [3. Usage](#3-usage)
    + [3.1 Start the node](#31-start-the-node)
    + [3.2 Container Status](#32-container-status)
    + [3.3 Check the logs](#33-check-the-logs)
    + [3.4 Open Nelson GUI](#34-open-nelson-gui)
  * [4. Warnings](#4-warnings)
    + [4.1 Ports](#41-ports)
    + [4.2 Hercules Remote API limits](#42-hercules-remote-api-limits)
  * [5. More information](#5-more-information)
  * [6. Author](#6-author)
  * [7. Special thanks to](#7-special-thanks-to)
  * [8. License](#8-license)
  * [9. Donations](#9-donations)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## 1. WARNING

I take no responsibility about eventual damage! This project includes following alpha and beta software:
* CarrIOTA Hercules
* CarrIOTA Nelson.cli
* CarrIOTA Nelson.gui
* CarrIOTA Field.cli

## 2. Install guide

### 2.1 Install Docker

- Install Docker (`docker-ce` or `docker.io`) and `docker-compose` from your OS package manager or directly from [docker.com](https://www.docker.com/get-docker).
- Add yourself to the docker group
```sh
sudo adduser ${USER} docker
```

### 2.2 Download this repository
Go to a directory and clone this repository (needs disk space because of IOTA mainnet database)
```sh
git clone https://gitlab.com/muXxer/docker-iota-hercules.git
cd docker-iota-hercules/
```

### 2.3 Change the IOTA configuration files to fit your needs 
#### 2.3.1 Change the Nelson config.ini

Edit the `volumes/nelson.cli/config.ini` file to match your needs, for example the name

```diff
[nelson]
-name = HerculesNode
+name = My awesome nelson node
```

#### 2.3.2 Change the Field config.ini

Edit the `volumes/field.cli/config.ini` file to match your needs, for example the name

```diff
[field]
-name = HerculesNode
+name = My awesome field node 
```

**Be sure to change your address field to your IOTA address for donations, otherwise thank you for leaving mine or add a new seed to get dynamically unused addresses *DO NOT USE YOUR MAIN WALLET SEED* **

Check your CarrIOTA Field node and donate to IOTA nodes here: http://field.carriota.com

### 2.4 Download the latest snapshot
To download the latest snapshot of the database (faster initial sync) use the following commands:
```sh
chmod +x download_latest_hercules_snapshot.sh
./download_latest_hercules_snapshot.sh
```

### 2.5 Open the following ports in your firewall for the IP address
- 14600 UDP - IOTA/IRI UDP connection port
- 15600 TCP - IOTA/IRI TCP connection port
- 16600 TCP - Nelson.cli TCP connection port
- 21310 TCP - Field.cli TCP connection port

## 3. Usage
### 3.1 Start the node

Enter the main docker-iota-hercules folder
```sh
cd docker-iota-hercules
```

Run it with:
```sh
docker-compose up -d
```

### 3.2 Container Status

Check if the docker container are up and running
```sh
docker ps
```

### 3.3 Check the logs

Check the IRI logs with
```sh
docker logs iota_hercules
```

Check the Nelson logs with
```sh
docker logs iota_nelson.cli
```

### 3.4 Open Nelson GUI

Open your browser to
```http
http://YOUR-IP:5000/
```

## 4. Warnings

### 4.1 Ports

The ports setup in the docker-compose.yml file opens following container ports

Port/Type | Use 
--- | ---
14265 | IOTA/IRI API port
14600/udp | IOTA/IRI UDP connection port
15600/tcp | IOTA/IRI TCP connection port
16600 | Nelson connection port
18600 | Nelson API port
21310 | CarrIOTA Field connection port
5000 | Nelson GUI

Please assure yourself to set your firewall accordingly, the ports are opened on 0.0.0.0 (all IP adresses, internal and external)

### 4.2 Hercules Remote API limits

**At this point NO API limits are now default!**

Following API limits are to be set as best practice (see iota.partners site or discussions on discord), but are not enabled as explained in the following table

parameter | explaination 
--- | ---
getNeighbors|No one can see the data of your neighbors
addNeighbors|No one can add neighbors to your node
removeNeighbors|No one can remove neighbors from your node
setApiRateLimit|This will prevent external connections from being able to use this command
interruptAttachingToTangle| To prevent users to do the PoW on your node
attachToTangle| To prevent users to do the PoW on your node

## 5. More information

For more information about the combined projects please refer to the following github repositories:

* [CarrIOTA Nelson client](https://github.com/SemkoDev/nelson.cli)
* [CarrIOTA Nelson GUI](https://github.com/SemkoDev/nelson.gui)
* [CarrIOTA Field client](https://github.com/SemkoDev/field.cli)


## 6. Author
* **muXxer**

## 7. Special thanks to 

* **Roman Semko** - [Twitter](https://twitter.com/romansemko) [GitHub](https://github.com/SemkoDev) - for beautiful software like Nelson.cli, Nelson.gui and Field.cli

## 8. License

This project is licensed under the ICS License - see the [LICENSE.md](LICENSE.md) file for details

## 9. Donations

**Buy me some beer**:

IOTA muXxer:
```raw
PHCIWQQIHQCAKPTJYXSRHIOSMZHHYVBBCALEBGXULYIAYJLBMFNAOHVJZUJZZZJXYUQSXHZZAKUBVLUDCE9AK9ZNVW
```